# UNEJ Minetest Game

The game used in the UNEJ experimentation.  

## Installation

- Unzip the archive, rename the folder to `unej` and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).

For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Compatibility

This game is compatible with the minetest version and deploiement found in the [UNEJ docker image](https://gitlab.com/iri-research-org/urbanum/minetest/docker-unej).

## Licensing

See `LICENSE.txt`

## List of mods

| NOM                    | SOURCE                                                                             | FORK                                                                   |
|------------------------|------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| abriglass              | https://github.com/Ezhh/abriglass                                                  |                                                                        |
| abripanes              | https://github.com/Ezhh/abripanes                                                  |                                                                        |
| antigrief              | https://bitbucket.org/sorcerykid/antigrief/src/master/                             |                                                                        |
| areas                  | https://github.com/minetest-mods/areas                                             |                                                                        |
| baked_clay             | https://notabug.org/TenPlus1/bakedclay                                             |                                                                        |
| basic_materials        | https://gitlab.com/VanessaE/basic_materials                                        |                                                                        |
| basic_signs            | https://gitlab.com/VanessaE/basic_signs                                            |                                                                        |
| beds                   | https://github.com/minetest-game-mods/beds                                         |                                                                        |
| bike                   | https://gitlab.com/h2mm/bike                                                       |                                                                        |
| binoculars             | https://github.com/minetest-game-mods/binoculars                                   |                                                                        |
| blockexport            | https://github.com/prestidigitator/minetest-mod-blockexport                        |                                                                        |
| boats                  | https://github.com/minetest-game-mods/boats                                        |                                                                        |
| bones                  | https://github.com/Bad-Command/bones                                               |                                                                        |
| bucket                 | https://github.com/minetest-game-mods/bucket                                       |                                                                        |
| butterflies            | https://github.com/minetest-game-mods/butterflies                                  |                                                                        |
| carts                  | https://github.com/minetest-game-mods/carts                                        |                                                                        |
| coloredwood            | https://gitlab.com/VanessaE/coloredwood                                            |                                                                        |
| creative               | https://github.com/minetest-game-mods/creative                                     |                                                                        |
| creative_areas         | https://github.com/kakalak-lumberJack/creative_areas                               |                                                                        |
| default                | https://github.com/minetest-game-mods/default                                      |                                                                        |
| display_modpack        | https://github.com/pyrollo/display_modpack                                         |                                                                        |
| doors                  | https://github.com/minetest-game-mods/doors                                        |                                                                        |
| dreambuilder_hotbar    | https://gitlab.com/VanessaE/dreambuilder_hotbar                                    |                                                                        |
| dye                    | https://github.com/minetest-game-mods/dye                                          |                                                                        |
| env_sounds             | https://github.com/minetest-game-mods/env_sounds                                   |                                                                        |
| farming                | https://github.com/minetest-game-mods/farming                                      |                                                                        |
| fireflies              | https://github.com/minetest-game-mods/fireflies                                    |                                                                        |
| flowers                | https://github.com/minetest-game-mods/flowers                                      |                                                                        |
| formspecs              | https://bitbucket.org/sorcerykid/formspecs/src/master/                             |                                                                        |
| game_commands          | https://github.com/minetest-mapserver/mapserver                                    |                                                                        |
| give_initial_stuff     | https://github.com/minetest-mapserver/mapserver_mod                                |                                                                        |
| hardenedclay           | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-hardenedclay.git |                                                                        |
| homedecor_modpack      | https://gitlab.com/VanessaE/homedecor_modpack                                      |                                                                        |
| ignfab                 | https://github.com/ignfab-minalac/minalac/tree/master/sources/src/main/java/ignfab |                                                                        |
| ilights                | https://gitlab.com/VanessaE/ilights                                                |                                                                        |
| itemshelf              | https://github.com/hkzorman/itemshelf                                              |                                                                        |
| laptop                 | https://github.com/Gerold55/laptop                                                 |                                                                        |
| lightnode              | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-lightnode.git    |                                                                        |
| liquid_restriction     | https://github.com/wsor4035/liquid_restriction                                     |                                                                        |
| magic_compass          | https://gitlab.com/zughy-friends-minetest/magic-compass                            | https://gitlab.com/iri-research-org/urbanum/minetest/magic-compass.git |
| map                    | https://github.com/minetest-game-mods/map                                          |                                                                        |
| mapserver              | https://github.com/minetest-mapserver/mapserver                                    |                                                                        |
| mapserver_mod          | https://github.com/minetest-mapserver/mapserver_mod                                |                                                                        |
| maptools               | https://github.com/minetest-mods/maptools                                          |                                                                        |
| mese_restriction       | https://github.com/Ezhh/mese_restriction                                           |                                                                        |
| mesecons               | https://github.com/minetest-mods/mesecons                                          |                                                                        |
| mesecons_x             | https://gitlab.com/deetmit/mesecons_x                                              |                                                                        |
| meshport               | https://github.com/random-geek/meshport                                            |                                                                        |
| minimap                |                                                                                    |                                                                        |
| monitoring             | https://github.com/minetest-monitoring/monitoring                                  |                                                                        |
| moreblocks             | https://github.com/minetest-mods/moreblocks/                                       |                                                                        |
| motorboat              | https://github.com/APercy/motorboat                                                |                                                                        | 
| orienteering           | https://repo.or.cz/minetest_orienteering.git                                       |                                                                        |
| perplayer_gamemode     | https://github.com/rubenwardy/perplayer_gamemode                                   |                                                                        |
| player_api             | https://github.com/minetest-game-mods/player_api                                   |                                                                        |
| playerfactions         | https://git.leagueh.xyz/katp32/playerfactions                                      |                                                                        |
| replacer               | https://github.com/Sokomine/replacer                                               |                                                                        |
| resize player          | https://github.com/LandonAConway/resize                                            |                                                                        |
| respawn                | https://gitlab.com/cronvel/mt-respawn                                              |                                                                        |
| rules                  | https://github.com/AiTechEye/agreerules                                            |                                                                        |
| sailing_kit            | https://github.com/TheTermos/sailing_kit                                           |                                                                        |
| screwdriver            | https://github.com/minetest-game-mods/screwdriver                                  |                                                                        |
| server_news            | https://github.com/Ezhh/server_news                                                |                                                                        |
| servercleaner          | https://github.com/AiTechEye/servercleaner/                                        |                                                                        |
| sethome                | https://github.com/minetest-game-mods/sethome                                      |                                                                        |
| sfinv                  | https://github.com/minetest-game-mods/sfinv                                        |                                                                        |
| sign_lib               | https://gitlab.com/VanessaE/signs_lib                                              |                                                                        |
| skinsdb                | https://github.com/minetest-mods/skinsdb                                           |                                                                        |
| spawn                  | https://github.com/minetest-game-mods/spawn                                        |                                                                        |
| stairs                 | https://github.com/minetest-game-mods/stairs                                       |                                                                        |
| travelnet              | https://github.com/Sokomine/travelnet                                              | https://github.com/mt-mods/travelnet                                   |
| ts_doors               | https://github.com/minetest-mods/ts_doors                                          |                                                                        |
| ts_furniture           | https://github.com/minetest-mods/ts_furniture                                      |                                                                        |
| ts_workshop            | https://github.com/minetest-mods/ts_workshop                                       |                                                                        |
| unej                   | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej             |                                                                        |
| unej_bim               | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej-bim         |                                                                        |
| unified_inventory      | https://github.com/minetest-mods/unified_inventory                                 |                                                                        |
| unified_inventory_plus | https://github.com/bousket/unified_inventory_plus                                  |                                                                        |
| unifieddyes            | https://gitlab.com/VanessaE/unifieddyes                                            |                                                                        |
| vessels                | https://github.com/minetest-game-mods/vessels                                      |                                                                        |
| walls                  | https://github.com/minetest-game-mods/walls                                        |                                                                        |
| we_undo                | https://github.com/HybridDog/we_undo                                               |                                                                        |
| weather                | https://github.com/minetest-game-mods/weather                                      |                                                                        |
| whitelist              | https://github.com/ShadowNinja/whitelist                                           |                                                                        |
| wield3d                | https://github.com/stujones11/wield3d                                              |                                                                        |
| windmill               | https://github.com/Sokomine/windmill                                               |                                                                        |
| wool                   | https://github.com/minetest-game-mods/wool                                         |                                                                        |
| worldedit              | https://github.com/Uberi/Minetest-WorldEdit                                        |                                                                        |
| xpanes                 | https://github.com/minetest-game-mods/xpanes                                       |                                                                        |

