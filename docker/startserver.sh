#!/usr/bin/env bash

set -euo pipefail

_esc_var() {
    printf '%s\n' "$@" | sed -e 's/[\/&]/\\&/g'
}

# copy the worldmods
if [[ ! -d /var/lib/minetest/worlds/world ]]; then
    mkdir -p /var/lib/minetest/worlds/world/worldmods
fi
pushd /usr/local/share/minetest/games/unej/worldmods
rsync -avhW --delete --no-compress --filter=". filter_rules.txt" * /var/lib/minetest/worlds/world/worldmods/
popd

# set the database info
# if the world.mt file exists and if the backend is postgres
# i.e. PG_HOST is set
world_mt_file="/var/lib/minetest/worlds/world/world.mt"
if [[ -f "$world_mt_file" ]]; then
    if $(grep -q -E "^gameid\s*=\s*.*$" "$world_mt_file"); then
        sed -i "s/^gameid[ ]*=[ ]*.*$/gameid = unej/g" $world_mt_file
    else
        echo "gameid = unej" >> $world_mt_file
    fi
fi

minetest_conf_file="/etc/minetest/minetest.conf"

if [[ ! -f "$minetest_conf_file" ]]; then
    cp /usr/local/share/doc/minetest/minetest.conf.example "$minetest_conf_file"
fi

magic_compass_config_file="/var/lib/minetest/worlds/world/worldmods/magic-compass/config.txt"
if [[ ! -f "$magic_compass_config_file" ]]; then
    cat > "$magic_compass_config_file" <<END-MC-CONFIG
menu_title =Retour à l'Agora

# you can add as many backgrounds/rows as you want, but be sure to edit load_config.lua too if you do so (EMPTY LINES ARE NOT COUNTED)
#menu_gui_bg_2rows =magiccompass_gui_bg_test2.png
#menu_gui_bg_4rows =magiccompass_gui_bg_test4.png
#menu_gui_bg_6rows =magiccompass_gui_bg_test6.png

menu_gui_button_bg =magiccompass_gui_button_bg.png
END-MC-CONFIG

fi

whitelist_file="/var/lib/minetest/worlds/world/whitelist.txt"
if [[ ! -f "$whitelist_file" ]]; then
    echo "admin" > "$whitelist_file"
fi

exec minetestserver \
    ${CLI_ARGS:-} \
    --config "/etc/minetest/minetest.conf" \
    --world "/var/lib/minetest/worlds/world" \
    --gameid unej
    --port 30000
