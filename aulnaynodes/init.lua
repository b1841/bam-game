minetest.register_node ("aulnaynodes:gravier_256", {
	description = "gravier 256 x 256 px",
    tiles = {"256_graviers.png"},
    groups = {cracky = 3},
})

minetest.register_node ("aulnaynodes:briques_256", {
	description = "briques 256 x 256 px",
    tiles = {"256_briques.png"},
    groups = {cracky = 3},
})

minetest.register_node ("aulnaynodes:mosaique_256", {
	description = "mosaique 256 x 256 px",
    tiles = {"256_mosaique.png"},
    groups = {cracky = 2},
})

minetest.register_node ("aulnaynodes:briques_rouge_256", {
	description = "briques_rouge 256 x 256 px",
    tiles = {"256_briques_rouges.png"},
    groups = {cracky = 1},
})